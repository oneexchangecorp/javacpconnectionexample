
import java.io.IOException;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

class CPConnection {

	static String domain = "REPLACE WITH CLIENT PORTAL API DOMAIN";
	static String username = "REPLACE WITH USERNAME";
	static String password = "REPLACE WITH PASSWORD";

	static String authToken = "";

	static WebsocketClient wsc;

	public static void main(String[] args) {

		authAndConnect();

	}

	private static void authAndConnect() {

		JSONObject credentials = new JSONObject();
		try {
			credentials.put("username", username);
			credentials.put("password", password);
		} catch (JSONException e1) {
			
			e1.printStackTrace();
		}

		StringEntity entity = new StringEntity(credentials.toString(), ContentType.APPLICATION_JSON);

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost request = new HttpPost("https://" + domain + "/api/login");
		request.setEntity(entity);

		HttpResponse response;
		try {
			response = httpClient.execute(request);

			String jsonString = EntityUtils.toString(response.getEntity());

			JSONTokener tokener = new JSONTokener(jsonString);

			try {
				JSONObject json = new JSONObject(tokener);

				authToken = json.get("token").toString();

				// Set Connect To Websocket
				wsc = new WebsocketClient(authToken, domain);

				// REST API CALLS
				getConfirmsByDateRange();

				getSettlesByDate();

				getIndicesByDate();

			} catch (JSONException e) {

				e.printStackTrace();
			}

		} catch (ClientProtocolException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	private static void getConfirmsByDateRange() {

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet("https://" + domain + "/api/confirms?from_date=02/07/2020&to_date=02/07/2020");

		request.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);

		try {
			HttpResponse response = httpClient.execute(request);

			System.out.println("********* Start getConfirmsByDateRange *********");

			String jsonString = EntityUtils.toString(response.getEntity());
			System.out.println(jsonString);

			System.out.println("********* End getConfirmsByDateRange *********");

		} catch (ClientProtocolException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	private static void getSettlesByDate() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet("https://" + domain + "/api/v1/settles?settle_date=06/17/2020");

		request.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);

		try {
			HttpResponse response = httpClient.execute(request);

			System.out.println("********* Start getSettlesByDate *********");

			String jsonString = EntityUtils.toString(response.getEntity());
			// System.out.println(jsonString);

			try {

				JSONArray jsonArray = new JSONArray(jsonString);

				// this is just to view in eclipse console as result is quite large
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject var = (JSONObject) jsonArray.get(i);

					System.out.println(var);

				}

			} catch (JSONException e) {

				e.printStackTrace();
			}

			System.out.println("********* End getSettlesByDate *********");

		} catch (ClientProtocolException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	private static void getIndicesByDate() {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet("https://" + domain + "/api/v1/cci_index?date=05/06/2020");

		request.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + authToken);

		try {
			HttpResponse response = httpClient.execute(request);

			System.out.println("********* Start getIndicesByDate *********");

			String jsonString = EntityUtils.toString(response.getEntity());


			try {

				JSONObject json = new JSONObject(jsonString);
				
				System.out.println(json);


			} catch (JSONException e) {
	
				e.printStackTrace();
			}

			System.out.println("********* End getIndicesByDate *********");

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}