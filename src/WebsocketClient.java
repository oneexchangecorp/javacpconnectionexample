import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

public class WebsocketClient extends Endpoint implements Runnable {

	

	public String apiKey = "";
	
	ClientEndpointConfig cec;
	WebSocketContainer container;
	
	String uri;

	public WebsocketClient(final String apiKey, String uri) {

		new Thread(this).start();

		this.apiKey = apiKey;
		
		this.uri = "wss://"+uri;

		try {
			 container = ContainerProvider.getWebSocketContainer();

			 cec = ClientEndpointConfig.Builder.create()
					.configurator(new ClientEndpointConfig.Configurator() {
						@Override
						public void beforeRequest(Map<String, List<String>> headers) {
							super.beforeRequest(headers);

							System.out.println("beforeRequest");
							List<String> cookieList = new ArrayList<String>();

							cookieList.add("token=" + apiKey);
							headers.put("Cookie", cookieList);

							System.out.println(cookieList);
						}

					}).build();

			

			 connect();

		} catch (Exception ex) {
			System.out.println(ex);
		}
	}
	
	private void connect()
	{
		try {
			
			container.connectToServer(this, cec, new URI(uri));
			
		} catch (DeploymentException e) {
			
			this.retryConnection();
			//e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}


	public void run() {
		while (true) {

			if (Thread.interrupted()) {
				return;
			}
		}

	}

	@Override
	public void onOpen(Session session, EndpointConfig config) {
		System.out.println("onOpen ");

		session.addMessageHandler(new MessageHandler.Whole<String>() {
			public void onMessage(String message) {
				System.out.println("Received message: " + message);
			}
		});

	}
	
	@Override
    public void onClose(Session session, CloseReason closeReason) 
	{
		System.out.println("onClose");
		
		retryConnection();
		

	}
	
	private void retryConnection()
	{
		try {
			TimeUnit.SECONDS.sleep(20);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connect();
	}
	
	
	
	

}